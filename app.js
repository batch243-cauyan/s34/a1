
const express = require('express');

// Create an application using express function
const app = express();

// For our application server to run, we need a port to listen to.
const port = 3000;
const { read } = require('fs');
let users = [{
    username: "johndoe",
    password: "johndoe1234"
},
{
    username: "secondUser",
    password: "extra"
}];

// Methods used from expressJS are middlewares
// API management is one of the common applications of middlewares
app.use(express.json()); // built-in middleware function of express that allows your app to read json data.


// Allows your app to read data from forms.
app.use(express.urlencoded({extended:true}));



app.get('/home', (req,res)=>{
    res.send('Welcome to the home page');
})


app.get('/users', (req,res)=>{
    res.send(users);
})



app.delete('/delete-user', (req,res)=>{

    let deletedUser = users.shift().username;
    res.send(`User ${deletedUser} has been deleted.`)
    console.log(users);
})

// delete specific user via index
app.delete('/delete-user/:userId', (req,res)=>{
    console.log(req.params);
    
    let deletedUser = users.splice(req.params.userId, 1);
    res.send(`User ${deletedUser.username} has been deleted.`)
    console.log(users);
})


app.listen(port, ()=> console.log(`Server started at port ${port}`));